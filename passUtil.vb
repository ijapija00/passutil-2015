﻿Imports System.Text
Imports System.Text.RegularExpressions
' Module Created 2015-10-25 18:04
' By @ijapija00
Module passUtil
    Private ReadOnly Property passwordRange As Long = 15 'should be between 1 and 180; put the value you want to use on overleap_h function
    Private ReadOnly Property sign As String = "*/passUtil/*"   ' signs passwords with *passUtil*
    Dim NPP0 As String = "" 'first int value
    Dim nPP As String = "" 'second int value
    Dim nPP2 As String = "" 'third int value
    Private ReadOnly Property maxLength As Integer = 21 ' overloaded value' if more you can split a string and then run this two times
    '    Private ReadOnly Property blocks write access to values from other forms or modules

    Private Sub HowToUse()
        'assign write_log with existing listbox or value "new listbox"
        'all logs will be written to this listbox ->
        '-> if you want export all logs to a text file with System.IO.StreamWriter
        'That´s it.
        'So just try and insert a listbox and you will see all results there
        ' use; string = passUtil.passUtil(string,integer)
    End Sub


#Region "proc_passUtil"
    Public Function run(importtxt As String, mode As Integer) ' handles process of passUtil
        Try
            Select Case mode 'pick from 0 and 1!
                Case 0 ' convert string to encrypted passcode
                    'check overloaded value

                    If importtxt.Length > maxLength Then 'check overload feature
                        log("")
                        log("---------ERROR---------")
                        Return "Overloaded value, max " + maxLength.ToString + "."
                    End If

                    If write_log.Items.Count = 0 Then
                    Else
                        log("")
                    End If
                    log("passUtil called with Success")
                    log("Working...")
                    nPP2 = reverse(importtxt, 0)
                    For Each tkn In nPP2
                        NPP0 = NPP0 + tkn + sign ' sign string to make impossible to return without formel
                    Next
                    nPP = bin(NPP0, 0)
                    nPP2 = bin(nPP, 0)
                    nPP = overleap_h(nPP2, 0)
                    log("passUtil Done!")
                    Return nPP
                    Exit Function
                Case 1 ' convert encrypted passcode to string with right settings; see (sign)

                    If write_log.Items.Count = 0 Then
                    Else
                        log("")
                    End If
                    log("passUtil called with Success")      'And (passwordRange)
                    log("Working...")
                    nPP = overleap_h(importtxt, 1)
                    nPP2 = bin(nPP, 1)
                    nPP = bin(nPP2, 1)
                    nPP2 = reverse(nPP.Replace(sign, ""), 1) ' removes sign string before reverse
                    log("passUtil Done!")
                    Return nPP2
                    Exit Function
            End Select
        Catch ex As Exception
            log("---------ERROR---------")
            Return "ERROR!"
            Exit Function
        End Try
        Return "ERROR!"
    End Function

#End Region
#Region "bin-func"
    Private Function bin(text_in As String, mode As Integer)
        Try
            Select Case mode
                Case 0 ' to binary
                    Dim Result As New System.Text.StringBuilder
                    Dim val As String = Nothing
                    For Each Character As Byte In System.Text.ASCIIEncoding.ASCII.GetBytes(text_in)
                        Result.Append(Convert.ToString(Character, 2).PadLeft(8, "0"))
                        Result.Append(" ")
                    Next
                    val = Result.ToString.Substring(0, Result.ToString.Length - 1)
                    log("Function bin returned success!")
                    Return val
                    Exit Function
                Case 1 ' from binary
                    Dim Characters As String = System.Text.RegularExpressions.Regex.Replace(text_in, "[^01]", "") 'fixes text_in value for byte later
                    Dim ByteArray((Characters.Length / 8) - 1) As Byte ' entering length of string / 8 + (-1)
                    Dim val As String = Nothing
                    For Index As Integer = 0 To ByteArray.Length - 1
                        ByteArray(Index) = Convert.ToByte(Characters.Substring(Index * 8, 8), 2)
                    Next
                    val = System.Text.ASCIIEncoding.ASCII.GetString(ByteArray)
                    log(("BIN : )" + "   LENGTH:" + nPP.Length.ToString))
                    Return val
                    Exit Function
            End Select
        Catch ex As Exception
            log("Function BIN returned ERROR!")
            Return "ERROR!"
            Exit Function
        End Try
        Return "ERROR!"
    End Function
#End Region
#Region "overleap_h function"
    Private Function overleap_h(ByVal text As String, mode As Integer) As String
        Try
            Dim v As Long = passwordRange
            Select Case mode
                Case 0 'encrypt
                    Dim total As String = ""
                    Dim tmp As String = ""
                    For i = 1 To Len(text)
                        tmp = Mid(text, i, 1)
                        tmp = Asc(tmp) + v
                        tmp = Chr(tmp)
                        total = total + tmp
                    Next i
                    log("overleap_h function returned success!")
                    log("Waiting for progress...")
                    Return total
                    Exit Function
                Case 1 ' decrypt
                    Dim total As String = ""
                    Dim tmp As String = ""
                    For i = 1 To Len(text)
                        tmp = Mid(text, i, 1)
                        tmp = Asc(tmp) - v
                        tmp = Chr(tmp)
                        total = total + tmp
                    Next i
                    log("overleap_h function returned success!")
                    log("Waiting for progress...")
                    Return total
                    Exit Function
            End Select
        Catch ex As Exception
            log("overleap_h returned ERROR!")
            Return "ERROR!"
            Exit Function
        End Try
        Return "ERROR!"
    End Function
#End Region

#Region "reverse-func"
    Private Function reverse(inputtext As String, mode As Integer)
        Dim tmp_key As New ListBox
        tmp_key.Items.Clear()
        Dim output_str As String = ""

        Select Case mode
            Case 0

                For Each key In inputtext
                    tmp_key.Items.Add(key)
                Next

                Dim openKey As Integer = tmp_key.Items.Count - 1
                For i = 0 To openKey + 1 'run from 0 to listbox count which means all tokens
                    tmp_key.SelectedIndex = openKey
                    output_str = output_str + tmp_key.SelectedItem
                    If openKey = 0 Then
                        Exit For
                    Else
                        openKey = openKey - 1
                    End If
                Next

                log("Function reverse returned Success!")
                Return output_str
            Case 1

                For Each key In inputtext
                    tmp_key.Items.Add(key)
                Next

                Dim openKey As Integer = tmp_key.Items.Count - 1
                For i = 0 To openKey + 1
                    tmp_key.SelectedIndex = openKey
                    output_str = output_str + tmp_key.SelectedItem
                    If openKey = 0 Then
                        Exit For
                    Else
                        openKey = openKey - 1
                    End If
                Next
                log("Function reverse returned Success!")
                Return output_str
        End Select

        Return "ERROR!"
        log("reverse returned ERROR!")
    End Function
#End Region
#Region "log"


    Private Sub log(container_log As String)
        dump(container_log, "0x0882")
    End Sub
    Private Property write_log As ListBox = New ListBox ' sync to listbox to see written logs during process
    Private Sub dump(b As String, intr As Object)
        Console.Write(intr)

        If b = "Dump Failed. Please assign a ListBox or Local File!" Then
            Exit Sub ' makes dump and log not loop if write_log = nothing
            ' as well as dumping log no where which means empty result in txtbox
        End If
        Try
            write_log.Items.Add(b)
        Catch ex As Exception
            Call log("Dump Failed. Please assign a ListBox or Local File!")
        End Try
        b = New String("")
    End Sub
#End Region
End Module
