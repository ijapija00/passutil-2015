# passUtil-2015
An awesome tool for encryption of passwords
Made in Visual Studio 2015

This module can be used in all kind of projects if you´re encrypting passwords or strings.
What makes this tool sooo special is that it uses four different encryption methods (processes) to build the final string.
If you want to check this out even more see: https://github.com/ijapija00/passUtil-2015/blob/master/example.txt

Feel free to modify and use!
How to use:

Import passUtil.vb into your project and after importing that add this code to your form code:

Example project,

imports {project_name}.passUtil

Public Class Form

Sub getpassword()

Dim value As String = ""

value = run(string, integer)

End Sub

End Class
__________________________________________
